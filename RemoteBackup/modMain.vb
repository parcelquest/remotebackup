Option Strict Off
Option Explicit On
Imports System.IO
Imports System.Threading
Imports System.Collections.Generic
Imports RemoteBackup.Utilities.FTP
Imports VB = Microsoft.VisualBasic

Module modMain
   Public g_sMachine As String
   Public gLogFile As String
   Public gOutputDir As String
   Public g_sToday As String
   Public g_sDate As String
   Public g_sYesterday As String
   Public g_sDayBefore As String

   Public gRemotePath As String
   Public gRemoteServ As String
   Public gUserName As String
   Public gPassword As String


   Public Sub Main()
      Dim sTmp As String
      Dim iRet As Integer
      Dim bManual As Boolean

      'Single instance only
      If (UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0) Then Exit Sub

      'Get INI settings
      If Not InitSettings() Then
         Exit Sub
      End If

      'Check command line
      sTmp = VB.Command()
      If InStr(sTmp, "-M") > 0 Then
         bManual = True
      Else
         bManual = False
      End If

      LogMsg("RemoteBackup " & My.Resources.Version & " - running on " & g_sMachine)
      Dim stopwatch As Stopwatch = stopwatch.StartNew()
      Thread.Sleep(0)
      stopwatch.Stop()
      Do
         iRet = MainProcess()
         'Exit loop if run in manual mode
         If bManual Or iRet = 0 Then
            Exit Do
         Else
            stopwatch.StartNew()
            Thread.Sleep(300000)    'Sleep 5 mins
            stopwatch.Stop()
         End If
      Loop Until iRet = 0

      LogMsg("Backup Job Done!")
   End Sub

   Private Function InitSettings() As Boolean
      Dim sTmp As String

      g_sToday = Format(Now, "ddd")
      gOutputDir = My.Settings.OutputDir
      g_sMachine = Environ("COMPUTERNAME")

      'For FTP
      gRemotePath = My.Settings.RemotePath
      gRemoteServ = My.Settings.FtpHost
      gUserName = My.Settings.UserName
      gPassword = My.Settings.Password

      If InStr(gOutputDir, "ddd") Then
         gOutputDir = Replace(gOutputDir, "ddd", g_sToday)
      End If

      sTmp = My.Settings.LogDir
      gLogFile = sTmp & "\RemoteBackup.log"

      If gOutputDir = "" Then
         InitSettings = False
      Else
         InitSettings = True
      End If
   End Function

   Public Sub LogMsg(ByVal strMsg As String)
      Dim ff As Integer

      Try
         ff = FreeFile()
         FileOpen(ff, gLogFile, OpenMode.Append)
         PrintLine(ff, Now & vbTab & strMsg)
         FileClose(ff)
      Catch
         Debug.WriteLine(strMsg)
      End Try
   End Sub

   'Given today date yyyymmdd, return yesterday yyyymmdd
   Public Function priorDate(ByVal sToday As String) As String
      Dim iMonth, iDay, iYear, lDate As Integer
      Dim sRet, sTime As String

      If sToday = "" Then
         sTime = Format(Today, "yyyyMMdd")
      ElseIf Len(sToday) < 8 Then
         priorDate = ""
         Exit Function
      Else
         sTime = sToday
      End If

      'Check for leap year
      iYear = Left(sTime, 4)
      If Right(sTime, 4) = "0301" Then
         If (iYear Mod 4) = 0 Then
            sRet = Left(sTime, 4) & "0229"
         Else
            sRet = Left(sTime, 4) & "0228"
         End If
      ElseIf Right(sTime, 2) = "01" Then
         iMonth = Mid(sTime, 5, 2)
         If iMonth = 1 Then
            iMonth = 12
            iYear = iYear - 1
         Else
            iMonth = iMonth - 1
         End If

         Select Case iMonth
            Case 1, 3, 5, 7, 8, 10, 12
               iDay = 31
            Case Else
               iDay = 30
         End Select

         If iMonth < 10 Then
            sRet = iYear & "0" & iMonth & iDay
         Else
            sRet = iYear & iMonth & iDay
         End If
      Else
         lDate = CInt(sTime)
         sRet = CStr(lDate - 1)
      End If

      priorDate = sRet
   End Function

   'Find the previous file that matches this template
   Private Function findPrevFile(ByVal sFilename As String, ByVal sCurDate As String, ByVal sPrevDate As String) As String
      Dim sRet, sTmp, sPriorDate As String
      Dim iCnt As Integer

      sRet = Replace(sFilename, sCurDate, sPrevDate)
      If File.Exists(sRet) Then
         findPrevFile = sRet
         Exit Function
      End If

      findPrevFile = ""

      'Check for existing file of the last 30 days
      sPriorDate = sPrevDate
      For iCnt = 1 To 30
         sTmp = priorDate(sPriorDate)
         sRet = Replace(sFilename, sCurDate, sTmp)
         If File.Exists(sRet) Then
            findPrevFile = sRet
            Exit For
         End If
         sPriorDate = sTmp
      Next

   End Function

   'This process is specifically designed for ParcelQuest use.  
   'It's not a general purpose ftp client
   Private Function MainProcess() As Integer
      Dim sDstFile, sSrcFile, sPrevFile, sSrcGrp, sFilename, sPrevDate, sCurDate As String
      Dim iRet, iGrpCnt, iFileCnt, lSrcTime, lDstTime As Integer
      Dim lSrcSize, lDstSize, lPrevSize As ULong
      Dim bRet As Boolean
      Dim dtSrcTime As DateTime

      'Set time frame for copy
      g_sDate = Format(Today, "yyyyMMdd")
      g_sYesterday = priorDate(g_sDate)
      g_sDayBefore = priorDate(g_sYesterday)

      'Get list of files to be downloaded
      Dim ftp As New FTPclient
      Dim fileList As New FTPdirectory

      ftp.Hostname = gRemoteServ
      ftp.Username = gUserName
      ftp.Password = gPassword
      sCurDate = g_sDate
      sPrevDate = g_sYesterday
      iRet = 0

      'Process each group of files separately
      For iGrpCnt = 1 To My.Settings.MaxGroup
         LogMsg("Start group: " & iGrpCnt)

         sSrcGrp = My.Settings("Grp" & iGrpCnt)
         If sSrcGrp <> "" Then
            If InStr(sSrcGrp, "yyyymmdd-") Then
               sSrcGrp = Replace(sSrcGrp, "yyyymmdd-", g_sYesterday)
               sCurDate = g_sYesterday
               sPrevDate = g_sDayBefore
            ElseIf InStr(sSrcGrp, "yyyymmdd") Then
               sSrcGrp = Replace(sSrcGrp, "yyyymmdd", g_sDate)
               sCurDate = g_sDate
               sPrevDate = g_sYesterday
            End If

            Try
               fileList = ftp.ListDirectoryDetail(sSrcGrp)
            Catch ex As Exception
               LogMsg("***** Error downloading: " & sSrcGrp & ".  Error= " & ex.Message())
               GoTo NextGrp
            End Try

            For iFileCnt = 0 To fileList.Count - 1
               sFilename = fileList(iFileCnt).Filename
               lSrcSize = fileList(iFileCnt).Size
               LogMsg("Checking: " & sFilename)
               sDstFile = gOutputDir & "\" & sFilename
               sSrcFile = gRemotePath & "/" & sFilename
               sPrevFile = findPrevFile(gOutputDir & "\" & sFilename, sCurDate, sPrevDate)

               lSrcTime = Format(fileList(iFileCnt).FileDateTime, "yyyyMMdd")
               dtSrcTime = ftp.GetFileDate(sSrcFile)
               lSrcTime = Format(dtSrcTime, "yyyyMMdd")
               If lSrcTime = 19000101 Then
                  LogMsg("*** Error accessing file: " & sSrcFile)
                  GoTo NextFile
               End If

               'Skip file
               If InStr(sFilename, My.Settings.SkipFile) Then
                  LogMsg("Skipping: " & sFilename)
                  GoTo NextFile
               End If

               'Check file exist
               If File.Exists(sDstFile) Then
                  lDstTime = Format(File.GetCreationTime(sDstFile), "yyyyMMdd")
                  lDstSize = My.Computer.FileSystem.GetFileInfo(sDstFile).Length
               Else
                  lDstTime = 0
                  lDstSize = 0
               End If
               If sPrevFile > " " And File.Exists(sPrevFile) Then
                  lPrevSize = My.Computer.FileSystem.GetFileInfo(sPrevFile).Length
               Else
                  lPrevSize = 0
               End If

               ' If src file is newer and size change from previous version, download it
               If lSrcTime > lDstTime And lSrcSize <> lDstSize And lSrcSize <> lPrevSize Then
                  LogMsg("Download --> " & sDstFile)

                  Try
                     bRet = ftp.Download(sSrcFile, sDstFile, True)

                     'Set original date for sDstFile
                     File.SetCreationTime(sDstFile, dtSrcTime)
                     iRet += 1
                  Catch ex As Exception
                     LogMsg("*** Fail downloading " & sDstFile & ". [Error: " & ex.Message() & "]")
                  End Try
               End If
NextFile:
            Next
         End If
NextGrp:
      Next
      LogMsg(iRet & " file(s) downloaded")
      MainProcess = iRet
   End Function

End Module